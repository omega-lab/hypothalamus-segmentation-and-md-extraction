#!/bin/bash
### based on fbeyer and adapted by chrschneider

##EXTRACT THE MD of the hypothalamus
#including only voxels with intensities lower than the mean of third ventricle)

## prereq software: freesurfer=6.0.0p1 fsl=5.0.11

subj_list="/data/pt_02020/MRI_data/scripts/subjects_incl_ids_completed.txt"
#subj_list="/data/pt_02020/MRI_data/scripts/wd_subjects_for_BIDS.txt"
missing_subj_list="/data/pt_02020/MRI_data/scripts/missing_MRI_datapoints.txt"
free_dir="/data/pt_02020/MRI_data/FREESURFER/"

filename_hyp="/data/pt_02020/MRI_data/DWI_preprocessed/MD/hypothalamus_md.txt"
filename_ventr="/data/pt_02020/MRI_data/DWI_preprocessed/MD/ventr_md.txt"
filename_hippo="/data/pt_02020/MRI_data/DWI_preprocessed/MD/hippocampus_md.txt"
subjdir="/data/pt_02020/MRI_data/DWI_preprocessed/MD/"

#delete old MD filelists and create new ones
rm -f $filename_hyp $filename_ventr $filename_hippo
touch $filename_hyp
touch $filename_ventr
touch $filename_hippo

echo "subj" "timepoint" "MD_median_hyp" "MD_mean_hyp" "MD_std_hyp" >> ${filename_hyp}
echo "subj" "timepoint" "MD_median_thirdventricle" "MD_mean_thirdventricle" "MD_std_thirdventricle" >> ${filename_ventr}
echo "subj" "timepoint" "MD_median_hippo_l" "MD_mean_hippo_l" "MD_std_hippo_l" "MD_median_hippo_r" "MD_mean_hippo_r" "MD_std_hippo_r" >> ${filename_hippo}



sed 1d $subj_list | while read line; do
  set -- $line

  subject=$(echo $2) 

#pilot participants (will not be used)
  if [[ $subject == pilot* ]]; then 
    continue 
  fi
  
  subject=$(echo $subject | grep -Eo '[0-9]{1,2}')
  subj="sub-"$subject
  
  for j in {01..04}; do
    tp="ses-"$j
    echo $subj $tp

    #if subj tp dwi in missing list -> skip
    if grep -P -q $subj"\ssess-"$j"\sdwi" "$missing_subj_list"; then
      continue
    fi

    mkdir -p ${subjdir}/${subj}/${tp}

    ##binarize the third ventricle mask
    #convert mgz to nii (with FS mri_convert)
    mri_convert -it mgz -i ${free_dir}/${subj}_${tp}.long.template_${subj}/mri/aseg.mgz -ot nii -o ${subjdir}/${subj}/${tp}/${subj}_aseg.nii.gz

    mri_convert -it mgz -i /data/pt_02020/MRI_data/Hypo_Seg/Hypo_Seg_DL/segmentation/${subj}_${tp}/T1_seg.mgz -ot nii -o /data/pt_02020/MRI_data/Hypo_Seg/Hypo_Seg_DL/segmentation/${subj}_${tp}/T1_seg.nii.gz

    #take the roi as a mask
    ## for 3rd ventricle
    fslmaths ${subjdir}/${subj}/${tp}/${subj}_aseg.nii.gz -uthr 14 -thr 14 -bin ${subjdir}/${subj}/${tp}/${subj}_thirdventricle.nii.gz
    ## for left hippocampus
    fslmaths ${subjdir}/${subj}/${tp}/${subj}_aseg.nii.gz -uthr 17 -thr 17 -bin ${subjdir}/${subj}/${tp}/${subj}_hippo_l.nii.gz
    ## for right hippocampus
    fslmaths ${subjdir}/${subj}/${tp}/${subj}_aseg.nii.gz -uthr 53 -thr 53 -bin ${subjdir}/${subj}/${tp}/${subj}_hippo_r.nii.gz

    ##combine transforms from MD to longitudinal timepoint

    #unzip source
    #echo /data/pt_02020/wd/hcp_prep/dwi_preproc/_subject_${subj}_${tp}/dti/dtifit__FA.nii.gz
    echo /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__FA.nii.gz
    gunzip -f /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__FA.nii.gz
    gunzip -f /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__MD.nii.gz

    lta_convert --inreg /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/fa2anat.dat  --outlta /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/fa2anat.lta -src /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__FA.nii -trg /data/pt_02020/MRI_data/FREESURFER/${subj}_${tp}/mri/norm.mgz

    mri_concatenate_lta /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/fa2anat.lta /data/pt_02020/MRI_data/FREESURFER/${subj}_${tp}.long.template_${subj}/mri/transforms/${subj}_${tp}_to_${subj}_${tp}.long.template_${subj}.lta ${subjdir}/${subj}/${tp}/${subj}_lta_final


    #convert the cross-sectional MD/FA maps to the longitudinal timepoint.
    mri_convert -at ${subjdir}/${subj}/${tp}/${subj}_lta_final /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__MD.nii ${subjdir}/${subj}/${tp}/${subj}_MD_${tp}.nii
    mri_convert -at ${subjdir}/${subj}/${tp}/${subj}_lta_final /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__FA.nii ${subjdir}/${subj}/${tp}/${subj}_FA_${tp}.nii

    #get average MD in third ventricle
    a="`fslstats ${subjdir}/${subj}/${tp}/${subj}_MD_${tp}.nii -k ${subjdir}/${subj}/${tp}/${subj}_thirdventricle.nii.gz  -p 50 -m -s`"

    set -- ${a}
    echo "threshold of ventricle: ${2}"

    echo $subj ${tp} $a >> ${filename_ventr}

    ## extract MD for hypothalamus
    # note: -p is percentile (50% percentile) threshold -m is mean -s is SD?
    hyp="`fslstats ${subjdir}/${subj}/${tp}/${subj}_MD_${tp}.nii -u ${2} -k /data/pt_02020/MRI_data/Hypo_Seg/Hypo_Seg_DL/segmentation/${subj}_${tp}/T1_seg.nii.gz -p 50 -m -s`" #-V

    echo $subj ${tp} $hyp >> ${filename_hyp}

    
    ### to check if correct: extract MD for hippocampus
    hippo_l="`fslstats ${subjdir}/${subj}/${tp}/${subj}_MD_${tp}.nii -u ${2} -k ${subjdir}/${subj}/${tp}/${subj}_hippo_l.nii.gz -p 50 -m -s`" #-V
    hippo_r="`fslstats ${subjdir}/${subj}/${tp}/${subj}_MD_${tp}.nii -u ${2} -k ${subjdir}/${subj}/${tp}/${subj}_hippo_r.nii.gz -p 50 -m -s`" #-V
    
    echo $subj ${tp} $hippo_l $hippo_r >> ${filename_hippo}

 
    gzip /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__FA.nii
    rm -rf /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__FA.nii
    gzip /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__MD.nii
    rm -rf /data/pt_02020/MRI_data/DWI_preprocessed/diffusion/${subj}/${tp}/dtifit__MD.nii
  done 
done

