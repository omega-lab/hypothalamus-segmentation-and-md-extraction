## connect to powerful server
getserver -LTs

### enable virtualenv
cd /data/pt_02020/MRI_data/scripts/DWI/1a_hypothalamus_seg/hyp_seg_deeplearning/virtualenv_hyp_seg/
source bin/activate

% to open script
cd /data/pt_02020/MRI_data/scripts/DWI/1a_hypothalamus_seg/hyp_seg_deeplearning/
python3 hyp_seg_for_GUT-BRAIN.py
