# CAVE: main() is commented right now !! un-comment if needed !!

## CAVE 2: go to server with large tmp -> getserver -sLT


### for testing single subject in terminal command line
## ./hypo_seg /data/pt_02020/MRI_data/FREESURFER/sub-01_ses-01.long.template_sub-01/mri/T1.mgz /data/pt_02020/MRI_data/Hypo_Seg/Hypo_Seg_DL/segmentation/ --out_posteriors /data/pt_02020/MRI_data/Hypo_Seg/Hypo_Seg_DL/posteriors/ --out_volumes /data/pt_02020/MRI_data/Hypo_Seg/Hypo_Seg_DL/segmentation/volume

# Import packages
from os import path, makedirs, listdir, chdir, rmdir, remove, rename
from pandas import read_csv, DataFrame  # for to_csv
import subprocess
from tqdm import tqdm
# from shutil import copy2
import sys
import glob

print('python version: ', sys.version)
# Preparation of segmentation
HYPO_SEG_PROGRAM = '/data/pt_02020/MRI_data/scripts/DWI/1a_hypothalamus_seg/hyp_seg_deeplearning/hypothalamus_seg/hypo_seg'
INPUT_DIR = '/data/pt_02020/MRI_data/FREESURFER/'
pp_list = listdir(INPUT_DIR)
pp_list_long = [s for s in pp_list if ".long" in s]
OUT_DIR = '/data/pt_02020/MRI_data/Hypo_Seg'
# create list with all T1 images and output folders
out_folder = path.join(OUT_DIR, 'Hypo_Seg_DL')
chdir(out_folder)

#### input location (FreeSurfer LONG output)
#### e.g. /data/pt_02020/MRI_data/FREESURFER/sub-01_ses-01.long.template_sub-01/mri/T1.mgz

def main():
    '''Runs segmentation program hypo_seg by Billet et al. 2020 and saves
    segmented images, posterior and volumes for each image'''
    # Initialize
    print('Processing images...')
    for pp in tqdm(pp_list_long):   ### to run for single subject add [:1] after pp_list_long
        # individual image to be segmented
        img = path.join(INPUT_DIR, pp, 'mri/T1.mgz')

        # create folder structure
        path_seg = path.join(out_folder, 'segmentation', pp.split(".")[0]) ### take only 0th part of subject-ID
        makedirs(path_seg, exist_ok=True)
        path_posterior = path.join(out_folder, 'posteriors', pp.split(".")[0])
        makedirs(path_posterior, exist_ok=True)
        path_volume = path.join(out_folder, 'volume', pp.split(".")[0])
        makedirs(path_volume, exist_ok=True)

        # uncomment to copy original file
        # copy2(img, path_seg)

        # command for segmentation
        args = [HYPO_SEG_PROGRAM, img, path_seg, '--out_posteriors',
                path_posterior, '--out_volumes', path_volume]
        subprocess.run(args,
                       stdout=subprocess.PIPE,  # capture output
                       stderr=subprocess.STDOUT,  # output and error together
                       universal_newlines=True)  # error captured as string
        # find command to save output/error in list and print after for loop
        # subprocess.call(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # remove unnecessary characters "." from file names
    old_names = glob.glob('/data/pt_02020/MRI_data/Hypo_Seg/Hypo_Seg_DL/**/*/T1._*')  
    [rename(file, file.replace('/T1._','/T1_')) for file in old_names]
    print('...done.')
    # somehow print errors


def mergeVolumeInfo():
    '''integegrates all single csv files with individual volumes into one csv 
    called "volume.csv" '''
    volume_files = [path.join(out_folder, 'volume',
                              pp.split(".")[0]) + '.csv' for pp in pp_list_long]
    my_csv = 'volumes.csv'

    with open(my_csv, 'w+') as file:
        for index, pp_path in enumerate(volume_files):
            with open(pp_path, 'r') as another:
                # skip header for all but the first file
                header = another.readline()  # strip away header
                if pp_path is volume_files[0]:
                    file.write(header)
                [file.write(f'{pp_list_long[index]}{line}') for line in another]
            # remove(pp_path)

    # delete "T1" in subject name and save again
    volumes = read_csv(my_csv)
    volumes["subject"] = volumes['subject'].str.replace('T1.mgz', '') ## TBD: adapt output string format
    # volumes["subject"] = volumes['subject'].str.replace('.long.template', '') ## TBD: adapt output string format
    volumes.to_csv(path.join(out_folder, my_csv), index=False)
    
    #### remove volume output per subject
    path_volume = path.join(out_folder, 'volume')
    import shutil
    shutil.rmtree(path_volume)


if __name__ == "__main__":
    # execute only if run as a script
    # main()
    mergeVolumeInfo()
