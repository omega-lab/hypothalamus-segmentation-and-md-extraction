### GUT-BRAIN Hypothalamus Segmentation Quality Control ####

## aim: to visually inspect results of hyp_seg and its co-registration to T1 images per individual per session
## prerequists: segmented hypothalamus.nifti
## next steps: MD extraction of hypothalamus using hypo_seg as brain mask

steps 
1) open terminal 
2) copy paste to command line + ENTER:

a) either in fsleyes
fsleyes /data/pt_02020/MRI_data/FREESURFER/sub-02_ses-02.long.template_sub-02/mri/T1.mgz /data/pt_02020/MRI_data/Hypo_Seg/Hypo_Seg_DL/segmentation/sub-02_ses-02/T1_seg.mgz -nc red-yellow

b) or in freeview
freeview /data/pt_02020/MRI_data/FREESURFER/sub-01_ses-01.long.template_sub-01/mri/T1.mgz /data/pt_02020/MRI_data/Hypo_Seg/Hypo_Seg_DL/segmentation/sub-01_ses-01/T1_seg.mgz:colormap=heat


3) change session to 02 or 03 or 04; change subject up to sub-61

4) note any observations as comments here:
/data/pt_02020/MRI_data/scripts/DWI/1a_hypothalamus_seg/hyp_seg_deeplearning/QA_comments.csv
